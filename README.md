# **Singleton Pattern** #
## What is it? ##
Singleton Pattern is a design pattern that limits the creation of instance of given object to **one**, meaning that there can only be one instance of the given object created by Java Virtual Machine, and make it accessible by every other classes. 

## How to use ##
**1)** We first make the constructor of the object private. This prevents every other instantiation from outside classes.



```
#!java

public class SingletonExample {
	private SingletonExample() {};
}

```


**2)** Create a public and static method (so it can be called without creating the object first) for accessing the one instance of this object and declare that one instance as a private and static attribute. There are many ways to declare the one-instance:

   **2.1)** Check if the one-instance is null. If it is, create that instance. This is called "Lazy Initialization" (not that it is bad though)

```
#!java

public class SingletonExample {
	private static SingletonExample instance;
	private SingletonExample() {};
	public static SingletonExample getInstance() {
		if (instance == null) instance = new SingletonExample();
		return instance;
	}
}

```

   **2.2)** Create the instance right at the attribute declaration and make it final. This is useful when the program always needs the instance and/or when resource and time are not a problem. This is called "Eager Initialization".

```
#!java

public class SingletonExample {
	private static final SingletonExample INSTANCE = new SingletonExample();
	private SingletonExample() {};
	public static SingletonExample getInstance() {
		return INSTANCE;
	}
}
```

   **2.3)** Use enumeration. Since in Java, all values of an enumeration is instantiated only once and accessible by all other classes. When you use the method, you don't need the attribute and the accessor anymore. You also don't need to prevent cloning. 

```
#!java

public enum SingletonExample {
	INSTANCE;
	private SingletonExample() {}
}

```

**3)** Override clone method to make this object unclonable by making it throws an exception. Note that if you use enumeration, you don't need to do this since enumerations are already unclonable)

```
#!java
	//add this method
	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

```

**4)** If you're using the Lazy Initialization method, make the instance accessor thread-safe by, for example, making it "synchronized" since the accessor might be accessed simultaneously and resulted in two instances being created.


```
#!java
	//note the keyword "synchronized"
	public static synchronized SingletonExample getInstance() {
		if (instance == null) instance = new SingletonExample();
		return instance;
	}

```


**5)** Then add whatever behaviors and attributes you want.


```
#!java

/**
 * An example of Singleton object. This uses Lazy Initialization method.
 * @author Pitchaya Namchaisiri
 * @author Woramate Jumroonsilp
 *
 */
public class SingletonExample {
	/**
	 * The one-instance. 
	 */
	private static SingletonExample instance;
	
	/**
	 * Whatever attribute for your object.
	 */
	private int whateverAttribute;
	
	/**
	 * The private constructor.
	 */
	private SingletonExample() {
		this.whateverAttribute = 0;
	}
	
	/**
	 * Grants other classes access to this one instance of this object. Creates one instance upon being called for the first time only.
	 * @return the only instance of this object.
	 */
	public static synchronized SingletonExample getInstance() {
		if (instance == null) instance = new SingletonExample();
		return instance;
	}
	
	/**
	 * Overrides the Object's clone method. Always throws CloneNotSupportedException to prevent cloning.
	 * @throws CloneNotSupportedException.
	 */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
    
    /**
     * Whatever behavior for your object.
     * @return whatever.
     */
    public int doSomething() {
    	return this.whateverAttribute;
    }
}


```